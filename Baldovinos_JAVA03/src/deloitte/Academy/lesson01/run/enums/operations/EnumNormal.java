package deloitte.Academy.lesson01.run.enums.operations;

enum Color {
	ROJO, VERDE, AZUL;
	
}


/**
 * @author jbaldovinos
 * Un ejemplo donde se declara enum fuera de
 * cualquier clase (Nota la palabra enum en
 * lugar de la pabra class
 */

public class EnumNormal {
/**
 * El metodo principal.
 * @param args
 */

	public static void main (String[] args) {
		Color color = Color.VERDE;
		System.out.println(color);		
		
	}

}
