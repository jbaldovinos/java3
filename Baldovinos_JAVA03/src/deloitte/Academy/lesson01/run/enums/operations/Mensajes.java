package deloitte.Academy.lesson01.run.enums.operations;

public enum Mensajes {
	OK("Ejecución con exito.",200), Error("Ejecución con errores",401);
	
	public String descripcion;
	public int codigo;

	
	private Mensajes(String descripcion, int codigo) {
		this.descripcion = descripcion;
		this.codigo = codigo;
		
	}

	
	
	private Mensajes() {
		// TODO Auto-generated constructor stub
		
	}
	public String getDescripcion() {
		return descripcion;
	}


	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
}

